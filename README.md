# fastapi-admin


#### 介绍

fastapi-admin 前端使用 Vue3 + Vite2 + ElementPlus ，后端使用 FastAPI + Sqlalchemy ，实现的项目快速开发的基础开发包

#### 安装教程

后端启动：


-      进入 server 目录下
-     python3 runme.py



前端启动：


-      进入 web 目录下
-      npm run dev


#### QQ群  940103666
![输入图片说明](image/QQ.png)
   

项目截图预览
![输入图片说明](image/%E5%9B%BE%E7%89%87.png)

![输入图片说明](image/QQ%E6%88%AA%E5%9B%BE20220117092740.png)

![输入图片说明](image/QQ%E6%88%AA%E5%9B%BE20220117092904.png)![输入图片说明](image/QQ%E6%88%AA%E5%9B%BE20220117092911.png)