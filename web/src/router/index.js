import {createRouter, createWebHistory, createWebHashHistory} from 'vue-router'
import { Session } from '@/api/session'
export const menulist=[
    {
        path: '/home',
        name: 'home',
        component: () => import('@/views/Home.vue'),
        meta: {
            title: '主页',
            roles: ['admin', 'common'],
            icon: 'House',
        },
    },
    {
        path: '/menu',
        name: 'menu',
        meta: {
            title: '菜单',
            roles: ['admin', 'common'],
            icon: 'Menu',
        },
        children:[
            {
                path: '/docs',
                name: 'docs',
                component: () => import('@/views/Docs.vue'),
                meta: {
                    title: 'Docs',
                    roles: ['admin'],
                    icon: 'ChatDotSquare',
                },
            },
            {
                path: '/redoc',
                name: 'redoc',
                component: () => import('@/views/Redoc.vue'),
                meta: {
                    title: 'redoc',
                    roles: ['admin', 'common'],
                    icon: 'HelpFilled',
                },
            },
        ]
    },
    {
        path: '/seting',
        name: 'seting',
        component: () => import('@/views/Seting.vue'),
        redirect: '/usermanage',
        meta: {
            title: '系统管理',
            roles: ['admin'],
            icon: 'setting',
        },
        children:[
            {
                path: '/usermanage',
                name: 'usermanage',
                component: () => import('@/views/User/UserManage.vue'),
                meta: {
                    title: '用户管理',
                    roles: ['admin'],
                    icon: 'UserFilled',
                },
            },
            {
                path: '/loginhistory',
                name: 'loginhistory',
                component: () => import('@/views/Loginhistory.vue'),
                meta: {
                    title: '登录历史',
                    roles: ['admin'],
                    icon: 'Tickets',
                },
            },
            {
                path: '/accress',
                name: 'accress',
                component: () => import('@/views/Accress.vue'),
                meta: {
                    title: '访问日志',
                    roles: ['admin'],
                    icon: 'Tickets',
                },
            },
    ]
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('@/views/About.vue'),
        meta: {
            title: '关于',
            roles: ['admin', 'common'],
            icon: 'InfoFilled',
        },
    },

]


const router = createRouter({
    history: createWebHashHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('@/views/login.vue')
        },
        {
            path: '/',
            name: '/',
            component: () => import('@/views/layout.vue'),
            redirect: '/home',
            meta: {
                isKeepAlive: true,
            },
            children: menulist
        },
    ]
})
router.beforeEach(async (to, from) => {
    const token = Session.get('token');
    const user = Session.get('user')
    if(!token||token=='undefined'||!user){
        if (to.path!='/login'){
            return '/login'
        }
    }else{
        if(to.path=='/login'){
            return '/'
        }
    }

})



export default router
